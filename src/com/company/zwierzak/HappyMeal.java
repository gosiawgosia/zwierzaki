package com.company.zwierzak;

public enum HappyMeal {

    MILK ("Mleko"),
    WATER ("Woda"),
    BONES ("Kości");

    private String name;

    HappyMeal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("HappyMeal [name %s]", this.name);
    }
}
