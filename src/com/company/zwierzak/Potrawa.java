package com.company.zwierzak;

public class Potrawa {

    private double amount;
    private HappyMeal potrawa;

    public Potrawa() {
    }

    public Potrawa(double amount, HappyMeal potrawa) {
        this.amount = amount;
        this.potrawa = potrawa;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public HappyMeal getPotrawa() {
        return potrawa;
    }

    public void setPotrawa(HappyMeal potrawa) {
        this.potrawa = potrawa;
    }

    @Override
    public String toString() {
        return String.format("Potrawa [amount %d, potrawa %s]",
                this.amount,
                this.potrawa);

    }
}
