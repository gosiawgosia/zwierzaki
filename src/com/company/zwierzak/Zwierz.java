package com.company.zwierzak;

public abstract class Zwierz {

    private String name;
    private int age;

    public Zwierz() {
    }

    public Zwierz(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Zwierz[name %s, age %]",
                this.name,
                this.age);
    }

    public abstract void dajGlos();

    public abstract void przedstawSie();
}
