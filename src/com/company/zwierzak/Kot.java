package com.company.zwierzak;

public class Kot extends Zwierz {

    public Kot() {
    }

    public Kot(String name, int age) {
        super(name, age);
    }

    @Override
    public void dajGlos() {
        System.out.println("Miau...miau");
    }

    @Override
    public void przedstawSie() {
        System.out.println("");

    }

}
