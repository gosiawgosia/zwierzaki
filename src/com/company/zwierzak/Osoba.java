package com.company.zwierzak;

//Napisz aplikację, która mam za zadanie stworzyć osobę, która będzie mogła posiadać zwierzaki róznego rodzaju.
// Każdy zwierzak ma swoją listę ulubionych potraw. Osoba ma listę dostęnych potraw. Nakarm wrzystkie zwierzaki.

public class Osoba {

    private String firstName;
    private String pesel;

    public Osoba() {

    }

    public Osoba(String firstName, String pesel) {
        this.firstName = firstName;
        this.pesel = pesel;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return String.format("Osoba[firstName: %s, pesel: %s]",
                this.firstName,
                this.pesel);
    }
}
